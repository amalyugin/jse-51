package ru.t1.malyugin.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    void closeEntityManagerFactory();

    @NotNull
    Liquibase getLiquibase();

    void close();

}