package ru.t1.malyugin.tm.api.repository.dto;

import ru.t1.malyugin.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

}
