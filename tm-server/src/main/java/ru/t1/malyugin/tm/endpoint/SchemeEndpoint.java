package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.ISchemeEndpoint;
import ru.t1.malyugin.tm.api.service.ISchemeService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeDropRequest;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeUpdateRequest;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeDropResponse;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeUpdateResponse;
import ru.t1.malyugin.tm.exception.server.EndpointException;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ISchemeEndpoint")
public final class SchemeEndpoint extends AbstractEndpoint implements ISchemeEndpoint {


    public SchemeEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    private ISchemeService getSchemeService() {
        return getServiceLocator().getSchemeService();
    }

    @NotNull
    @Override
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeDropRequest request
    ) {
        @Nullable final String initToken = request.getInitToken();
        try {
            getSchemeService().dropScheme(initToken);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

    @NotNull
    @Override
    public SchemeUpdateResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeUpdateRequest request
    ) {
        @Nullable final String initToken = request.getInitToken();
        try {
            getSchemeService().updateScheme(initToken);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeUpdateResponse();
    }

}