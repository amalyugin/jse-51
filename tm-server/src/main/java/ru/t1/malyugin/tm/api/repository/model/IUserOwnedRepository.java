package ru.t1.malyugin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

    void clear(@NotNull String userId);

    void update(@NotNull String userId, @NotNull M model);

}
