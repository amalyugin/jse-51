package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.api.service.model.ITaskService;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IUserDTOService getUserDTOService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    ISchemeService getSchemeService();

}