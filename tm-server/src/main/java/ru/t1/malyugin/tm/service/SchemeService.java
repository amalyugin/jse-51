package ru.t1.malyugin.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.ISchemeService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.exception.user.PermissionException;

public final class SchemeService implements ISchemeService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SchemeService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }


    @Override
    @SneakyThrows
    public void dropScheme(@Nullable final String initToken) {
        if (StringUtils.isBlank(initToken)) throw new PermissionException();
        @NotNull final String token = getPropertyService().getInitToken();
        if (!StringUtils.equals(initToken, token)) throw new PermissionException();
        @NotNull final Liquibase liquibase = getConnectionService().getLiquibase();
        liquibase.dropAll();
        liquibase.close();
    }

    @Override
    @SneakyThrows
    public void updateScheme(@Nullable final String initToken) {
        if (StringUtils.isBlank(initToken)) throw new PermissionException();
        @NotNull final String token = getPropertyService().getInitToken();
        if (!StringUtils.equals(initToken, token)) throw new PermissionException();
        @NotNull final Liquibase liquibase = getConnectionService().getLiquibase();
        liquibase.update("scheme");
        liquibase.close();
    }

}