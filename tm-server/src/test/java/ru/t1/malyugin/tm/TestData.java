package ru.t1.malyugin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface TestData {

    int NUMBER_OF_PROJECTS = 10;

    int NUMBER_OF_TASKS = 10;

    int NUMBER_OF_USERS = 10;

    @NotNull
    String FIRST_USUAL_USER_LOGIN = "TST_USER_1";

    @NotNull
    String FIRST_USUAL_USER_PASS = "TST_PASS_1";

    @NotNull
    String SECOND_USUAL_USER_LOGIN = "TST_USER_2";

    @NotNull
    String SECOND_USUAL_USER_PASS = "TST_PASS_2";

    @NotNull
    String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    String UNKNOWN_STRING = "SOME_STR";

    @NotNull
    List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    @NotNull
    List<TaskDTO> TASK_LIST = new ArrayList<>();

    @NotNull
    List<UserDTO> USER_LIST = new ArrayList<>();

}