package ru.t1.malyugin.tm.dto.response.system;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;


@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @Nullable
    private String serverName;

    @Nullable
    private String authorName;

    @Nullable
    private String authorEmail;

    @Nullable
    private String gitBranch;

    @Nullable
    private String gitCommitId;

    @Nullable
    private String gitCommitterName;

    @Nullable
    private String gitCommitterEmail;

    @Nullable
    private String gitMessage;

    @Nullable
    private String gitTime;

}