package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start task by id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");

        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken(), id);
        getTaskEndpoint().startTaskById(request);
    }

}