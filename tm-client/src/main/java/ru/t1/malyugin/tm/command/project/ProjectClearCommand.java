package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-clear";

    @NotNull
    private static final String DESCRIPTION = "Clear all projects";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProject(request);
    }

}