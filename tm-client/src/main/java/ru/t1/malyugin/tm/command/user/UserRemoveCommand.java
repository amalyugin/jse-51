package ru.t1.malyugin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.user.UserRemoveRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-remove";

    @NotNull
    private static final String DESCRIPTION = "remove user profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");

        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        getUserEndpoint().removeUser(request);
    }

}