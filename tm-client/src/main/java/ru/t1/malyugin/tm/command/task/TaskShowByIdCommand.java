package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Show task by id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");

        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @Nullable final TaskDTO task = getTaskEndpoint().showTaskById(request).getTask();
        renderTask(task);
    }

}