package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String DESCRIPTION = "Show version info";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final String serverVersion = getSystemEndpoint().getVersion(request).getVersion();
        @Nullable final String clientVersion = getPropertyService().getApplicationVersion();
        System.out.println("CLIENT VERSION: " + clientVersion);
        System.out.println("SERVER VERSION: " + serverVersion);
    }

}