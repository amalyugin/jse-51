package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.TaskShowListByProjectIdRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK LIST BY PROJECT ID]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskShowListByProjectIdRequest request = new TaskShowListByProjectIdRequest(getToken(), projectId);
        @Nullable final List<TaskDTO> tasks = getTaskEndpoint().showTaskListByProject(request).getTaskList();
        renderTaskList(tasks);
    }

}